# Web Service Project

Tools needed to run the program:

* Eclipse EE (I use Oxygen)
* Java 8 (version 9 still makes problems)
* MongoDB https://www.mongodb.com/download-center#community
* Project Lombok (install Eclipse first) https://projectlombok.org/download
    
Usefull tools:

* Robomongo for browsing the database https://robomongo.org/
* Postman for testing https://www.getpostman.com/
    
On first run:

* create a mongodb database called "ws"
* run gradle in Eclipse first
* then deploy on tomcat server (I use v 9.0)
    
Production Server:
* http://46.101.106.26:8080

##### Project structure:
- restLayer:
    - All the API Resources for all Classes
    - Resource.java provides CRUD operations for all Resources and some helper methods
- model:
    - All models, thanks to MongoDB + project lombok very minimal
- db:
    - A Repository for every class
    - You can directly use java classes for some DB operations
    - Repository.java provides common DB operations for CRUD operations
