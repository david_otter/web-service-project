package restLayer;

import javax.ws.rs.Path;
import businessLayer.db.ParkingRepository;
import businessLayer.models.Parking;
import businessLayer.models.UserType;

/**
 * Full CRUD operation support
 * 
 * @author David Otter
 *
 */
@Path("/parkings")
public class ParkingResource extends Resource<Parking> {
	
	private ParkingRepository parkingRepository = new ParkingRepository();
	
	public ParkingResource() {
		this.setRepository(parkingRepository);
		this.setTemplate(new Parking());
	}
	
	protected boolean isComplete(Parking parking) {
		return parking.getDescription() != null && parking.getAvailable() != null && parking.getFee() != null && parking.getMallId() != null && parking.getSlots() != null;
	}
	
	protected boolean authorizeDelete(Parking parking) {
		return getTokenOwner().getUserType().equals(UserType.ADMIN.toString());
	}

	@Override
	protected boolean authorizePut(Parking generic) {
		return true;
	}

	@Override
	protected boolean authorizePost(Parking parking) {
		return true;
	}

}
