package restLayer;

import java.security.Key;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.core.Response.Status;

import businessLayer.db.UserRepository;
import businessLayer.models.Id;
import businessLayer.models.User;
import businessLayer.models.UserType;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import restLayer.jwt.JWTKeyGenerator;
import static javax.ws.rs.core.HttpHeaders.AUTHORIZATION;
import static javax.ws.rs.core.MediaType.APPLICATION_FORM_URLENCODED;
import static javax.ws.rs.core.Response.Status.UNAUTHORIZED;

/**
 * Full CRUD operation support
 * Uses JWT to generate a Bearer token on login
 * JWT token part based on:
 * 			Antonio Goncalves
 *			http://www.antoniogoncalves.org
 * 
 * @author David Otter
 *
 */
@Path("/users")
public class UserResource extends Resource<User> {
	
	private Key key = JWTKeyGenerator.getKey();
	
	@Context
	private UriInfo uriInfo;
	
	private UserRepository userRepository = new UserRepository();
	
	public UserResource() {
		this.setRepository(userRepository);
		this.setTemplate(new User());
	}
	
	// Do not return passwords even if they are hashed ...
	protected Object stripAttributes(Object response) {
		if (response instanceof User) {
			((User)response).setPassword(null);
		}
		return new Container((Id) response);
	}
	
	protected boolean isComplete(User user) {
		return user.getEmail() != null && user.getName() != null && user.getPassword() != null;
	}
	
	@POST
    @Path("/login")
    @Consumes(APPLICATION_FORM_URLENCODED)
    public Response authenticateUser(@FormParam("login") String login,
                                     @FormParam("password") String password) {
        try {
            // Authenticate the user using the credentials provided
        	User user = new User();
        	user.setId(login);
        	user = userRepository.getById(user);
        	if (user == null) {
        		return Response.status(Status.NOT_FOUND).build();
        	}
            if (!user.authenticate(password)) {
            	return Response.status(Status.UNAUTHORIZED).build();
            }
            // Issue a token for the user
            String token = issueToken(login);
            // Return the token on the response
            return Response.ok().header(AUTHORIZATION, "Bearer " + token).build();
        } catch (Exception e) {
            return Response.status(UNAUTHORIZED).build();
        }
	}
	
	/**
	 * No authentication on creating a new user
	 */
	@POST
	@Consumes({ MediaType.APPLICATION_JSON})
	@Produces({ MediaType.APPLICATION_JSON})
	public Response create(User user) {
		return super.create(user);
	}
	
	private String issueToken(String login) {
        String jwtToken = Jwts.builder()
                .setSubject(login)
                .setIssuer(uriInfo.getAbsolutePath().toString())
                .setIssuedAt(new Date())
                .setExpiration(toDate(LocalDateTime.now().plusMinutes(240L)))
                .signWith(SignatureAlgorithm.HS512, key)
                .compact();
        return jwtToken;
	}
	
	private Date toDate(LocalDateTime localDateTime) {
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
	}
	
	protected boolean authorizeDelete(User user) {
		if (getSecurityContext().getUserPrincipal().getName().equals(user.getId())) {
			return true;
		}
		return getTokenOwner().getUserType().equals(UserType.ADMIN.toString());
	}
	
	protected boolean authorizePut(User user) {
		return authorizeDelete(user);
	}

	@Override
	protected boolean authorizePost(User user) {
		return true;
	}
	

}
