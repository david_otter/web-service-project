package restLayer;

import businessLayer.models.*;

public class Container {
	private class Link {
		@SuppressWarnings("unused")
		public String rel;
		@SuppressWarnings("unused")
		public String href;
		
		public Link(String type, String href) {
			this.rel = type;
			this.href = href;
		}
	}
	
	public Id object;
	public Link[] links;
	
	public Container(Id object) {
		this.object = object;
		if (object instanceof User) {
			this.links = userLinks(object);
		}	else	{
			if (object instanceof Shop) {
				this.links = shopLinks(object);
			}	else	{
				if (object instanceof Queue) {
					this.links = queueLinks(object);
				}	else	{
					if (object instanceof Parking) {
						this.links = parkingLinks(object);
					}	else	{
						if (object instanceof Mall) {
							this.links = mallLinks(object);
						}
					}
				}
			}
		}
	}
	
	private Link[] userLinks(Id object) {
		Link[] links = new Link[2];
		
		links[0] = new Link("self", "http://46.101.106.26:8080/WebServiceProject/rest/users/" + object.getId());
		links[1] = new Link("login", "http://46.101.106.26:8080/WebServiceProject/rest/users/login");
		
		return links;
	}
	
	private Link[] shopLinks(Id object) {
		Link[] links = new Link[3];
		
		links[0] = new Link("self", "http://46.101.106.26:8080/WebServiceProject/rest/shops/" + object.getId());
		links[1] = new Link("queues", "http://46.101.106.26:8080/WebServiceProject/rest/shops/" + object.getId() + "/queues");
		links[2] = new Link("fastest-queue", "http://46.101.106.26:8080/WebServiceProject/rest/shops/" + object.getId() + "/queues/fastest");
		
		return links;
	}
	
	private Link[] mallLinks(Id object) {
		Link[] links = new Link[5];
		
		links[0] = new Link("self", "http://46.101.106.26:8080/WebServiceProject/rest/malls/" + object.getId());
		links[1] = new Link("parking", "http://46.101.106.26:8080/WebServiceProject/rest/malls/" + object.getId() + "/parkings");
		links[2] = new Link("shops", "http://46.101.106.26:8080/WebServiceProject/rest/malls/" + object.getId() + "/shops");
		links[3] = new Link("category-shops", "http://46.101.106.26:8080/WebServiceProject/rest/malls/" + object.getId() + "/shops/{category}");
		links[4] = new Link("parkings-mode", "http://46.101.106.26:8080/WebServiceProject/rest/malls/" + object.getId() + "/parkings/{mode}");
		
		return links;
	}
	
	private Link[] queueLinks(Id object) {
		Link[] links = new Link[1];
		
		links[0] = new Link("self", "http://46.101.106.26:8080/WebServiceProject/rest/queues/" + object.getId());
		
		return links;
	}
	
	private Link[] parkingLinks(Id object) {
		Link[] links = new Link[1];
		
		links[0] = new Link("self", "http://46.101.106.26:8080/WebServiceProject/rest/parkings/" + object.getId());
		
		return links;
	}
	
}
