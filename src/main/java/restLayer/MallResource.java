package restLayer;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import businessLayer.db.MallRepository;
import businessLayer.db.ParkingRepository;
import businessLayer.db.ShopRepository;
import businessLayer.models.Mall;
import businessLayer.models.Parking;
import businessLayer.models.Shop;
import businessLayer.models.UserType;
import restLayer.jwt.JWTTokenNeeded;

/**
 * Full CRUD operation support
 * 
 * @author David Otter
 *
 */
@Path("/malls")
public class MallResource extends Resource<Mall> {
	
	private MallRepository mallRepository = new MallRepository();
	private ShopRepository shopRepository = new ShopRepository();
	private ParkingRepository parkingRepository = new ParkingRepository();
	
	public MallResource() {
		this.setRepository(mallRepository);
		this.setTemplate(new Mall());
	}
	
	protected boolean isComplete(Mall mall) {
		return mall.getName() != null && mall.getOpeningHours() != null && mall.getCategories() != null && mall.getOwnerId() != null;
	}

	protected boolean authorizeDelete(Mall mall) {
		mall = mallRepository.getById(mall);
		if (mall != null && getTokenOwner().getId().equals(mall.getOwnerId())) {
			return true;
		}
		return getTokenOwner().getUserType().equals(UserType.ADMIN.toString());
	}
	
	protected boolean authorizePost(Mall mall) {
		return getTokenOwner().getUserType().equals(UserType.ADMIN.toString()) || getTokenOwner().getUserType().equals(UserType.SHOPOWNER.toString());
	}
	
	@Override
	protected boolean authorizePut(Mall generic) {
		return true;
	}
	
	@GET
	@JWTTokenNeeded
	@Path("{id}/shops")
	@Produces({ MediaType.APPLICATION_JSON})
	public Response getShopsByID(@PathParam("id") String id) {
		List<Shop> shops = shopRepository.getByMallId(id);
		if (shops == null) {
    		return Response.status(Status.NOT_FOUND).build();
    	}
		List<Container> response = new ArrayList<Container>();
		for(Shop q : shops) {
			response.add((Container) stripAttributes(q));
		}
		return Response.ok(response).build();
	}

	
	@GET
	@JWTTokenNeeded
	@Path("{id}/parkings")
	@Produces({ MediaType.APPLICATION_JSON})
	public Response getParkingsByID(@PathParam("id") String id) {
		List<Parking> parkings = parkingRepository.getByMallId(id);
		if (parkings == null) {
    		return Response.status(Status.NOT_FOUND).build();
    	}
		List<Container> response = new ArrayList<Container>();
		for(Parking q : parkings) {
			response.add((Container) stripAttributes(q));
		}
		return Response.ok(response).build();
	
		
	}
	
	@GET
	@JWTTokenNeeded
	@Path("{id}/shops/{category}")
	@Produces({ MediaType.APPLICATION_JSON})
	public Response getShopsByIDAndCategory(@PathParam("id") String id, @PathParam("category") String category) {
		List<Shop> shops = shopRepository.getByMallId(id);
		if (shops == null) {
    		return Response.status(Status.NOT_FOUND).build();
    	}
		List<Container> response = new ArrayList<Container>();
		for(Shop q : shops) {
			List<String>categories = q.getCategories();
			if (categories.contains(category)) response.add((Container) stripAttributes(q));
		}
		return Response.ok(response).build();
	}
	
	@GET
	@JWTTokenNeeded
	@Path("{id}/parkings/{mode}")
	@Produces({ MediaType.APPLICATION_JSON})
	public Response getParkingsByIDAndMode(@PathParam("id") String id, @PathParam("mode") String mode) {
		//mode can be ‘free’ (max number of free spaces), 
		//‘ratio’ (best ratio of free to taken spaces) or 
		//‘price’ (minimizing of course)
		List<Parking> parkings = parkingRepository.getByMallId(id);
		if (parkings == null) {
    		return Response.status(Status.NOT_FOUND).build();
    	}
		List<Container> response = new ArrayList<Container>();
		int availableSpaces = 0;
		double ratio = Integer.MAX_VALUE;
		double fee = Integer.MAX_VALUE;
		for(Parking p : parkings) {
			if (mode.equals("free")) {
				int n = p.getAvailable();
				if (n > availableSpaces) {
					response.removeAll(response);
					response.add((Container) stripAttributes(p));
					availableSpaces = n;
				}
			}
			if (mode.equals("ratio")) {
				double n = p.getAvailable()/(p.getSlots() - p.getAvailable());
				if (n < ratio) {
					response.removeAll(response);
					response.add((Container) stripAttributes(p));
					ratio = n;
				}
			}
			if (mode.equals("price")) {
				double n = p.getFee();
				if (n < fee) {
					response.removeAll(response);
					response.add((Container) stripAttributes(p));
					fee = n;
				}
			}
			response.add((Container) stripAttributes(p));
		}
		return Response.ok(response).build();
	
		
	}
	
}
