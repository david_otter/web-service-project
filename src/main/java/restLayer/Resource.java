package restLayer;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;


import businessLayer.db.Repository;
import businessLayer.db.UserRepository;
import businessLayer.models.Id;
import businessLayer.models.User;
import restLayer.jwt.JWTTokenNeeded;

/**
 * Template for all Resources with full CRUD support
 * Use return response in subclasses
 * Override if a CRUD operation does not fit
 * Most complicated Class of this project as it uses a lot of Generic, interfaces, inheritance ...
 * 
 * @author David Otter
 *
 * @param <T> is the class you are returning with the specific Controller
 * e.g. for UserResource it is user
 */
public abstract class Resource <T extends Id> {
	
	private Repository<T> repository;
	private T template;
	@Context
	private SecurityContext securityContext;
	
	protected SecurityContext getSecurityContext() {
		return securityContext;
	}
	
	protected void setRepository(Repository<T> repository) {
		this.repository = repository;
	}
	
	protected void setTemplate(T template) {
		this.template = template;
	}
	
	/**
	 * Handles all internal errors (code 500)
	 * Returns the object (code 200)
	 * or just the response code if no object exists (code 404)
	 */
	protected Response returnResponse(Function<T, Object> function, T generic, boolean noStrip) {
		Object response;
		try {
			response = function.apply(generic);
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
		if (response == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		if (noStrip) {
			return Response.ok(response).build();
		}
		return Response.ok(stripAttributes(response)).build();
	}
	
	protected Response returnResponse(Function<T, Object> function, T generic) {
		return this.returnResponse(function, generic, false);
	}
	
	/**
	 * Can be overridden if needed e.g. for user
	 */
	protected Object stripAttributes(Object response) {
		return new Container((Id) response);
	}
	
	/**
	 * Use it for checking if there is a bad request (code 400) 
	 */
	protected abstract boolean isComplete(T generic);
	
	/*
	 * Authorize put, post, delete is used for advanced role based authorization 
	 */
	
	protected abstract boolean authorizePut(T generic);
	
	protected abstract boolean authorizePost(T generic);
	
	protected abstract boolean authorizeDelete(T generic);
	
	/**
	 * Role based access:
	 * 	As a admin you can do everything!
	 * 	User: delete + put only by the user itself, post has no authentication at all
	 * 	Mall: delete only by owner, post only by role shopowner
	 * 	Shop: delete only by owner, post only by role shopowner
	 * 	Parking: delete only by role admin
	 * 	Queue: delete only by role admin
	 * 	Every other method has no role based access control (still valid jwt token is needed)
	 */
	
	/**
	 * JWT is used on every API endpoint but creating a new user and login (of course)
	 */
	
	/**
	 * Handy method to find the role of a user
	 */
	protected User getTokenOwner() {
		try {
			User tokenOwner = new User();
			tokenOwner.setId(getSecurityContext().getUserPrincipal().getName());
			tokenOwner = new UserRepository().getById(tokenOwner);
			return tokenOwner;
		} catch (Exception e) {
			e.printStackTrace();
		}
		// Should never happen as this method is called after JWT test passed !!
		return null;
	}
	
	// It is possible to update selective only some attributes
	@PUT @Path("{id}")
	@JWTTokenNeeded
	@Consumes({ MediaType.APPLICATION_JSON})
	@Produces({ MediaType.APPLICATION_JSON})
	public Response update(T generic, @PathParam("id") String id) {
		generic.setId(id);
		if (!authorizePut(generic)) {
			return Response.status(Status.UNAUTHORIZED).build();
		}
		return returnResponse(repository::update, generic);
	}
	
	@GET
	@JWTTokenNeeded
    @Produces({ MediaType.APPLICATION_JSON })
    public Response getAll() {
		List<T> documents = repository.getAll();
		if (documents.isEmpty()) {
    		return Response.status(Status.NOT_FOUND).build();
    	}
		List<Container> response = new ArrayList<Container>();
		for(T t : documents) {
			response.add((Container) stripAttributes(t));
		}
		
		return Response.ok(response).build();
    }
	
	@GET @Path("{id}")
	@JWTTokenNeeded
    @Produces({ MediaType.APPLICATION_JSON })
    public Response getById(@PathParam("id") String id) {
		template.setId(id);
		return returnResponse(repository::getById, template);
    }
	
	// Returns only the id
	@DELETE @Path("{id}")
	@JWTTokenNeeded
	@Produces({ MediaType.APPLICATION_JSON })
	public Response deleteById(@PathParam("id") String id) {
		template.setId(id);
		if (!authorizeDelete(template)) {
			return Response.status(Status.UNAUTHORIZED).build();
		}
		return returnResponse(repository::deleteById, template, true);
	}
	
	@POST
	@JWTTokenNeeded
	@Consumes({ MediaType.APPLICATION_JSON})
	@Produces({ MediaType.APPLICATION_JSON})
	public Response create(T generic) {
		if (!isComplete(generic)) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		if (!authorizePost(generic)) {
			return Response.status(Status.UNAUTHORIZED).build();
		}
		return returnResponse(repository::create, generic);
	}

}
