package restLayer;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import businessLayer.db.QueueRepository;
import businessLayer.db.ShopRepository;
import businessLayer.models.Queue;
import businessLayer.models.Shop;
import businessLayer.models.UserType;
import restLayer.jwt.JWTTokenNeeded;

/**
 * Full CRUD operation support
 * 
 * @author David Otter
 *
 */
@Path("/shops")
public class ShopResource extends Resource<Shop> {
	
	private ShopRepository shopRepository = new ShopRepository();
	private QueueRepository queueRepository = new QueueRepository();
	
	public ShopResource() {
		this.setRepository(shopRepository);
		this.setTemplate(new Shop());
	}
	
	protected boolean isComplete(Shop shop) {
		return new MallResource().isComplete(shop) && shop.getLatitude() != null && shop.getLongitude() != null && shop.getMallId() != null;
	}
	
	protected boolean authorizeDelete(Shop shop) {
		shop = shopRepository.getById(shop);
		if (shop != null && getTokenOwner().getId().equals(shop.getOwnerId())) {
			return true;
		}
		return getTokenOwner().getUserType().equals(UserType.ADMIN.toString());
	}
	
	protected boolean authorizePost(Shop shop) {
		return getTokenOwner().getUserType().equals(UserType.ADMIN.toString()) || getTokenOwner().getUserType().equals(UserType.SHOPOWNER.toString());
	}
	
	@Override
	protected boolean authorizePut(Shop generic) {
		return true;
	}
	
	@GET
	@JWTTokenNeeded
	@Path("{id}/queues")
	@Produces({ MediaType.APPLICATION_JSON})
	public Response getShopQueues(@PathParam("id") String id) {

		List<Queue> queues = queueRepository.getByShopId(id);
		if (queues == null) {
    		return Response.status(Status.NOT_FOUND).build();
    	}
		List<Container> response = new ArrayList<Container>();
		for(Queue q : queues) {
			response.add((Container) stripAttributes(q));
		}
		return Response.ok(response).build();
	}
	
	@GET
	@JWTTokenNeeded
	@Path("{id}/queues/fastest")
	@Produces({ MediaType.APPLICATION_JSON})
	public Response getShopFastestQueues(@PathParam("id") String id) {
		List<Queue> queues = queueRepository.getByShopId(id);
		if (queues == null) {
    		return Response.status(Status.NOT_FOUND).build();
    	}
		List<Container> response = new ArrayList<Container>();
		 Integer max = Integer.MAX_VALUE;
		for(Queue q : queues) {
			Integer n = q.getWaitingTime();
			if (n < max && q.getIsOpen()) {
				response.removeAll(response);
				response.add((Container) stripAttributes(q));
				max = n;
			}
			else if (n == max && q.getIsOpen()) {
				
				response.add((Container) stripAttributes(q));
			}
		}
		return Response.ok(response).build();
	}
		
	@GET
	@JWTTokenNeeded
	@Path("category/{category}")
	@Produces({ MediaType.APPLICATION_JSON})
	public Response getShopByCategory(@PathParam("category") String category) {
		System.out.println(shopRepository);
		List<Shop> shops = shopRepository.getByCategory(category);
		if (shops == null) {
    		return Response.status(Status.NOT_FOUND).build();
    	}
		List<Container> response = new ArrayList<Container>();
		for(Shop q : shops) {
			response.add((Container) stripAttributes(q));
		}
		return Response.ok(response).build();
	}
	
	@GET
	@JWTTokenNeeded
	@Path("timelimit/{timelimit}")
	@Produces({ MediaType.APPLICATION_JSON})
	public Response getShopTimelimit(@PathParam("timelimit") int timelimit) {
		List<Shop> shops = shopRepository.getAll();
		if (shops.isEmpty()) {
    		return Response.status(Status.NOT_FOUND).build();
    	}
		List<Container> response = new ArrayList<Container>();
		
		for(Shop s : shops) {
			List<Queue> shopQueues = queueRepository.getByShopId(s.getId());
			boolean added = false;
			for (Queue q: shopQueues) {
				Integer n = q.getWaitingTime();
				if (n <= timelimit && !added) {
					response.add((Container) stripAttributes(s));
					added = true;
				}
			}
			
		}	
				
		return Response.ok(response).build();
	}
	
}