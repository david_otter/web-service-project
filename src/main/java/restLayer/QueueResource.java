package restLayer;

import javax.ws.rs.Path;
import businessLayer.db.QueueRepository;
import businessLayer.models.Queue;
import businessLayer.models.UserType;

/**
 * Full CRUD operation support
 * 
 * @author David Otter
 *
 */
@Path("/queues")
public class QueueResource extends Resource<Queue> {
	
	private QueueRepository queueRepository = new QueueRepository();
	
	public QueueResource() {
		this.setRepository(queueRepository);
		this.setTemplate(new Queue());
	}
	
	protected boolean isComplete(Queue queue) {
		return queue.getDescription() != null && queue.getLength() != null && queue.getThroughput() != null && queue.getWaitingTime() != null && queue.getIsOpen() != null && queue.getShopId() != null;
	}
	
	protected boolean authorizeDelete(Queue queue) {
		return getTokenOwner().getUserType().equals(UserType.ADMIN.toString());
	}

	@Override
	protected boolean authorizePut(Queue generic) {
		return true;
	}

	@Override
	protected boolean authorizePost(Queue queue) {
		return true;
	}

}
