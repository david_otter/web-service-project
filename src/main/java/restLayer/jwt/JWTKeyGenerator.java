package restLayer.jwt;

import java.security.Key;
import java.security.NoSuchAlgorithmException;

import javax.crypto.KeyGenerator;

/**
 * 
 * @author David Otter
 *
 */
public class JWTKeyGenerator {
	
	private static Key key;

	public static Key getKey() {
		if (key == null) {
			try {
				key = KeyGenerator.getInstance("AES").generateKey();
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}
		}
		return key;
	}
}
