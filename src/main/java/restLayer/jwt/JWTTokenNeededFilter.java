package restLayer.jwt;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;

import javax.annotation.Priority;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.security.Key;
import java.security.Principal;

/**
 * Based on:
 * @author Antonio Goncalves
 *         http://www.antoniogoncalves.org
 *         --
 */
@Provider
@JWTTokenNeeded
@Priority(Priorities.AUTHENTICATION)
public class JWTTokenNeededFilter implements ContainerRequestFilter {
	
    private Key key = JWTKeyGenerator.getKey();

    // ======================================
    // =          Business methods          =
    // ======================================

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {

        // Get the HTTP Authorization header from the request
        String authorizationHeader = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);

        // Check if the HTTP Authorization header is present and formatted correctly
        if (authorizationHeader == null || !authorizationHeader.startsWith("Bearer ")) {
            throw new NotAuthorizedException("Authorization header must be provided");
        }

        // Extract the token from the HTTP Authorization header
        String token = authorizationHeader.substring("Bearer".length()).trim();

        try {

            // Validate the token
        	Jws<Claims> jwt = Jwts.parser().setSigningKey(key).parseClaimsJws(token);
        
	        /**
	         * Based on:
	         * 		https://stackoverflow.com/questions/26777083/best-practice-for-rest-token-based-authentication-with-jax-rs-and-jersey
	         */
	        final String id = jwt.getBody().getSubject();
	        final SecurityContext currentSecurityContext = requestContext.getSecurityContext();
	        requestContext.setSecurityContext(new SecurityContext() {
	        	@Override
	            public Principal getUserPrincipal() {
	        		return () -> id;
	            }
	
	            @Override
	            public boolean isUserInRole(String role) {
	                return currentSecurityContext.isUserInRole(role);
	            }
	
	            @Override
	            public boolean isSecure() {
	                return currentSecurityContext.isSecure();
	            }
	
	            @Override
	            public String getAuthenticationScheme() {
	                return currentSecurityContext.getAuthenticationScheme();
	            }
	        });
        
        } catch (Exception e) {
            requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
        }
    }
}
