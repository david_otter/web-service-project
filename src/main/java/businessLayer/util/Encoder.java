package businessLayer.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Encoder {

	public static String encode(String str) {
		try {
			byte[] buffer = str.getBytes();
			byte[] result = null;
			StringBuffer buf = null;
			MessageDigest md = MessageDigest.getInstance("SHA-512");
			//allocate room for the hash 
			result = new byte[md.getDigestLength()];
			//calculate hash 
			md.reset();
			md.update(buffer);

			result = md.digest();
			System.out.println(result);
			//create hex string from the 16-byte hash 
			buf = new StringBuffer(result.length * 2);
			for (int i = 0; i < result.length; i++) {
				int intVal = result[i] & 0xff;
				if (intVal < 0x10) {
					buf.append("0");
				}
				buf.append(Integer.toHexString(intVal));
			}
			return buf.toString();
		} catch (NoSuchAlgorithmException e) {
		}
		return null;
	}

}
