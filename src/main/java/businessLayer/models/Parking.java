package businessLayer.models;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

/**
 * The parking class with all attributes
 * Project Lombok auto-generates all Getters and Setters
 * 
 * @author David Otter
 *
 */
public class Parking implements Id {
	
	@Getter @Setter
	private String id;
	@Getter @Setter
	private String description;
	@Getter @Setter
	private Date updatedAt;
	@Getter @Setter
	private Integer slots;
	@Getter @Setter
	private Integer available;
	@Getter @Setter
	private Double fee;
	@Getter @Setter
	private String mallId;
}
