package businessLayer.models;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

/**
 * 
 * @author David Otter
 *
 */
public class Queue implements Id {
	
	@Getter @Setter
	private String id;
	@Getter @Setter
	private String description;
	@Getter @Setter
	private String shopId;
	@Getter @Setter
	private Date updatedAt;
	@Getter @Setter
	private Integer length;
	@Getter @Setter
	private Double throughput;
	@Getter @Setter
	private Integer waitingTime;
	@Getter @Setter
	private Boolean isOpen;
}