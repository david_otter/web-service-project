package businessLayer.models;

/**
 * 
 * @author David Otter
 *
 */
public interface Id {
	
	public String getId();
	public void setId(String id);

}
