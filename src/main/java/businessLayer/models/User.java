package businessLayer.models;

import businessLayer.util.Encoder;
import lombok.Getter;
import lombok.Setter;

/**
 * The user class with all attributes
 * Project Lombok auto-generates all Getters and Setters
 * 
 * @author David Otter
 *
 */
public class User implements Id {

	@Getter @Setter
	private String id;
	@Getter @Setter
	private String name;
	@Getter @Setter
	private String password;
	@Getter @Setter
	private String email;
	@Getter @Setter
	private String userType;
	
	public boolean authenticate(String password) {
		return this.password.equals(Encoder.encode(password));
	}
}
