package businessLayer.models;

import lombok.Getter;
import lombok.Setter;

/**
 * 
 * @author David Otter
 *
 */
public class Shop extends Mall {
	
	@Getter @Setter
	private String mallId;
	@Getter @Setter
	private Double longitude;
	@Getter @Setter
	private Double latitude;
}
