package businessLayer.models;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * 
 * @author David Otter
 *
 */
public class Mall implements Id {
	
	@Getter @Setter
	private String id;
	@Getter @Setter
	private String name;
	@Getter @Setter
	private String openingHours;
	@Getter @Setter
	private List<String> categories;
	@Getter @Setter
	private String ownerId;
}
