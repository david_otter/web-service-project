package businessLayer.db;

import org.bson.Document;
import com.mongodb.client.MongoCollection;
import businessLayer.models.User;
import businessLayer.models.UserType;
import businessLayer.util.Encoder;

/**
 * 
 * @author David Otter
 *
 */
public class UserRepository extends Repository<User> {
	
	private MongoCollection<User> collection = getMongoDb().getCollection("users", User.class);
	
	public UserRepository() {
		this.setCollection(collection);
	}

	@Override
	protected Document prepareUpdate(Document document, User user) {
		if (user.getPassword() != null) {
			document = document.append("password", Encoder.encode(user.getPassword()));
		}
		if (user.getEmail() != null) {
			document = document.append("email", user.getEmail());
		}
		if (user.getName() != null) {
			document = document.append("name", user.getName());
		}
		return document;
	}
	
	@Override
	protected User prepareCreate(User user) {
		user.setPassword(Encoder.encode(user.getPassword()));
		user.setUserType(UserType.USER.toString());
		return user;
	}

}
