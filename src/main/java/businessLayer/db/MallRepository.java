package businessLayer.db;

import org.bson.Document;
import com.mongodb.client.MongoCollection;

import businessLayer.models.Mall;

/**
 * 
 * @author David Otter
 *
 */
public class MallRepository extends Repository<Mall> {
	
	private MongoCollection<Mall> collection = getMongoDb().getCollection("malls", Mall.class);
	
	public MallRepository() {
		this.setCollection(collection);
	}

	@Override
	protected Document prepareUpdate(Document document, Mall mall) {
		if (mall.getName() != null) {
			document = document.append("name", mall.getName());
		}
		if (mall.getOpeningHours() != null) {
			document = document.append("openingHours", mall.getOpeningHours());
		}
		if (mall.getCategories() != null) {
			document = document.append("categories", mall.getCategories());
		}
		if (mall.getOwnerId() != null) {
			document = document.append("ownerId", mall.getOwnerId());
		}
		return document;
	}
}
