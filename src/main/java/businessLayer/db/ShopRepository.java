package businessLayer.db;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bson.Document;

import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;

import businessLayer.models.Shop;

/**
 * 
 * @author David Otter
 *
 */
public class ShopRepository extends Repository<Shop> {
	
	private MongoCollection<Shop> collection = getMongoDb().getCollection("shops", Shop.class);
	
	public ShopRepository() {
		this.setCollection(collection);
	}

	@Override
	protected Document prepareUpdate(Document document, Shop shop) {
		document = new MallRepository().prepareUpdate(document, shop);
		if (shop.getMallId() != null) {
			document = document.append("mallId", shop.getMallId());
		}
		if (shop.getLongitude() != null) {
			document = document.append("longitude", shop.getLongitude());
		}
		if (shop.getLatitude() != null) {
			document = document.append("latitude", shop.getLatitude());
		}
		return document;
	}
	

	public List<Shop> getByMallId(String id) {
		BasicDBObject query = new BasicDBObject("mallId", id);
		List<Shop> shops = new ArrayList<Shop>();
		try (MongoCursor<Shop> cursor = collection.find(query).iterator()) {
		    while (cursor.hasNext()) {
		        shops.add(cursor.next());
		    }
		}
		
		return shops;
	}
	
	public List<Shop> getByCategory(String category) {
		
		BasicDBObject query = new BasicDBObject("categories", new BasicDBObject("$in", Arrays.asList(category)));
		List<Shop> shops = new ArrayList<Shop>();
		try (MongoCursor<Shop> cursor = collection.find(query).iterator()) {
		    while (cursor.hasNext()) {
		        shops.add(cursor.next());
		    }
		}
		
		return shops;
	}
}
