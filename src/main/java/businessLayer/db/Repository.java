package businessLayer.db;

import java.util.ArrayList;
import java.util.List;

import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;

import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.model.FindOneAndUpdateOptions;
import com.mongodb.client.model.ReturnDocument;

import businessLayer.models.Id;

/**
 * All CRUD operations but update should be pretty straight forward
 * You can directly use the object for creating
 * 
 * @author David Otter
 *
 */
public abstract class Repository <T extends Id> extends DbProvider {
	
	private MongoCollection<T> collection;
	// needed to get "instant update results"
	protected FindOneAndUpdateOptions options = new FindOneAndUpdateOptions().returnDocument(ReturnDocument.AFTER);
	
	protected void setCollection(MongoCollection<T> collection) {
		this.collection = collection;
	}
	
	
	public List<T> getAll() {
		List<T> documents = new ArrayList<T>();
		try (MongoCursor<T> cursor = collection.find().iterator()) {
		    while (cursor.hasNext()) {
		        documents.add(cursor.next());
		    }
		}
		
		return documents;
	}
	
	public T getById(T generic) {
		BasicDBObject query = new BasicDBObject("_id", generic.getId());
		return collection.find(query).first();
	}
	
	public T create(T generic) {
		generic = prepareCreate(generic);
		generic.setId(new ObjectId().toHexString());
		collection.insertOne(generic);
		return generic;
	}
	
	public Bson deleteById(T generic) {
		Bson filter = new Document("_id", generic.getId());
		generic = collection.findOneAndDelete(filter);
		return generic == null ? null : new Document("id", generic.getId());
	}
	
	public T update(T generic) {
		Bson filter = new Document("_id", generic.getId());
		Document document = prepareUpdate(new Document(), generic);
		// This is what the MongoDriver wants if using findOneAndUpdate ...
		Bson update = new Document("$set", document);
		// replaceOne can directly use the object (much easier) but you have to do a "get" first to have all information
		return collection.findOneAndUpdate(filter, update, options);
	}
	
	/**
	 * Implement this to create the update Document
	 */
	protected abstract Document prepareUpdate(Document document, T generic);
	
	/**
	 * can be overridden to do something before creating
	 */
	protected T prepareCreate(T generic) {
		return generic;
	}

}
