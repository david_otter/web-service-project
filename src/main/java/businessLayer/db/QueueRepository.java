package businessLayer.db;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.bson.Document;

import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;

import businessLayer.models.Queue;

/**
 * 
 * @author David Otter
 *
 */
public class QueueRepository extends Repository<Queue> {
	
	private MongoCollection<Queue> collection = getMongoDb().getCollection("queues", Queue.class);
	
	public QueueRepository() {
		this.setCollection(collection);
	}

	@Override
	protected Document prepareUpdate(Document document, Queue queue) {
		if (queue.getDescription() != null) {
			document = document.append("description", queue.getDescription());
		}
		if (queue.getShopId() != null) {
			document = document.append("shopId", queue.getShopId());
		}
		if (queue.getLength() != null) {
			document = document.append("length", queue.getLength());
		}
		if (queue.getThroughput() != null) {
			document = document.append("throughput", queue.getThroughput());
		}
		if (queue.getWaitingTime() != null) {
			document = document.append("waitingTime", queue.getWaitingTime());
		}
		if (queue.getIsOpen() != null) {
			document = document.append("isOpen", queue.getIsOpen());
		}
		return document.append("updatedAt", new Date());
	}
	
	@Override
	protected Queue prepareCreate(Queue queue) {
		queue.setUpdatedAt(new Date());
		return queue;
	}
	
	public List<Queue> getByShopId(String id) {
		BasicDBObject query = new BasicDBObject("shopId", id);
		List<Queue> queues = new ArrayList<Queue>();
		try (MongoCursor<Queue> cursor = collection.find(query).iterator()) {
		    while (cursor.hasNext()) {
		        queues.add(cursor.next());
		    }
		}
		
		return queues;
				
	}
}
