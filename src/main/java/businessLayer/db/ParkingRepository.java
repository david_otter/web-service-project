package businessLayer.db;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.bson.Document;

import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;

import businessLayer.models.Parking;

/**
 * 
 * @author David Otter
 *
 */
public class ParkingRepository extends Repository<Parking> {
	
	private MongoCollection<Parking> collection = getMongoDb().getCollection("parkingLots", Parking.class);
	
	public ParkingRepository() {
		this.setCollection(collection);
	}

	@Override
	protected Document prepareUpdate(Document document, Parking parking) {
		if (parking.getDescription() != null) {
			document = document.append("description", parking.getDescription());
		}
		if (parking.getAvailable() != null) {
			document = document.append("parking", parking.getAvailable());
		}
		if (parking.getFee()!= null) {
			document = document.append("fee", parking.getFee());
		}
		if (parking.getMallId()!= null) {
			document = document.append("mallId", parking.getMallId());
		}
		if (parking.getSlots()!= null) {
			document = document.append("slots", parking.getSlots());
		}
		return document.append("updatedAt", new Date());
	}
	
	@Override
	public Parking prepareCreate(Parking parking) {
		parking.setUpdatedAt(new Date());
		return parking;
	}

	public List<Parking> getByMallId(String id) {
		BasicDBObject query = new BasicDBObject("mallId", id);
		List<Parking> parkings = new ArrayList<Parking>();
		try (MongoCursor<Parking> cursor = collection.find(query).iterator()) {
		    while (cursor.hasNext()) {
		        parkings.add(cursor.next());
		    }
		}
		
		return parkings;
	}

}
