package businessLayer.db;

import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.bson.codecs.configuration.CodecRegistries.fromProviders;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.client.MongoDatabase;

/**
 * DbProvider returns a mongoDB connection
 * All collections are automatically setup on first start
 * But the database ws has to exist
 * 
 * @author David Otter
 *
 */
public class DbProvider {
	
	private MongoDatabase db;
	
	@SuppressWarnings("resource")
	public MongoDatabase getMongoDb(){
        if(db == null) {
        	CodecRegistry pojoCodecRegistry = fromRegistries(MongoClient.getDefaultCodecRegistry(),
                    fromProviders(PojoCodecProvider.builder().automatic(true).build()));
        	MongoClient mongoClient = new MongoClient( "localhost:27017", MongoClientOptions.builder().codecRegistry(pojoCodecRegistry).build());
    		db = mongoClient.getDatabase( "ws" );
    		setup();
        }
        return db;
    }
	
	private void setup() {
		List<String> coll = new ArrayList<String>(Arrays.asList("users", "malls", "parkingLots", "queues", "shops"));
		for (String name : db.listCollectionNames()) {
			if (coll.contains(name)) {
				coll.remove(name);
			}
		}
		for (String name : coll) {
			db.createCollection(name);
		}
	}

}
